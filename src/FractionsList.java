import java.util.*;

public class FractionsList {
    List<Fraction> list = new ArrayList<>();
    Fraction cachedMin, cachedMax;

    void addFraction(Fraction fraction) {
        list.add(fraction);
        if (cachedMax != null) {
            if (fraction.compareTo(cachedMax) > 0) {
                cachedMax = fraction;
            }
        }
        if (cachedMin != null) {
            if (fraction.compareTo(cachedMin) < 0) {
                cachedMin = fraction;
            }
        }
    }

    FractionsList(FractionsList original) {
        this.list = new ArrayList<>(original.list);
    }

    FractionsList() {
    }

    Fraction minFraction() {
        if (cachedMin == null) {
            cachedMin = Collections.min(list);
        }
        return cachedMin;
    }

    Fraction maxFraction() {
        if (cachedMax == null) {
            cachedMax = Collections.max(list);
        }
        return cachedMax;
    }

    int countGreaterThan(Fraction fraction) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(fraction) > 0) {
                count++;
            }
        }
        return count;
    }

    int countLessThan(Fraction fraction) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(fraction) < 0) {
                count++;
            }
        }
        return count;
    }

    int size() {
        return list.size();
    }

    Fraction get(int index) {
        return list.get(index);
    }

    Fraction set(int index, Fraction el) {
        return list.set(index, el);
    }

    boolean add(Fraction el) {
        return list.add(el);
    }
}

