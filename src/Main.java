import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("input.txt"))) {
            List<Integer> numbers = new ArrayList<>();
            while (scanner.hasNextInt()) {
                numbers.add(scanner.nextInt());
            }
            System.out.println(numbers);
            FractionsList fractionsList4 = new FractionsList();
            for (int i = 0; i < numbers.size(); i += 2) {
                fractionsList4.addFraction(new Fraction(numbers.get(i), numbers.get(i + 1)));
            }

            System.out.println("Max: " + fractionsList4.maxFraction());
            System.out.println("Min: " + fractionsList4.minFraction());
            System.out.println("Count greater than 5: " + fractionsList4.countGreaterThan(new Fraction(4, 1)));
            System.out.println("Count less than 5: " + fractionsList4.countLessThan(new Fraction(4, 1)));
            System.out.println("fractionsList4:");
            for (Fraction elem : fractionsList4.list) {
                System.out.println(elem);
            }
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Fraction fraction1 = new Fraction(3, 8);
        Fraction fraction2 = new Fraction(1, 2);
        FractionsList fractionsList2 = new FractionsList();
        fractionsList2.addFraction(fraction2);
        fractionsList2.addFraction(fraction1);
        fractionsList2.addFraction(new Fraction(1, 3));
        fractionsList2.addFraction(new Fraction(1, 25));
        fractionsList2.addFraction(new Fraction(4, 2));
        fractionsList2.addFraction(new Fraction(16, 3));
        fractionsList2.addFraction(new Fraction(15, 1));
        System.out.println("Max: " + fractionsList2.maxFraction());
        System.out.println("Min: " + fractionsList2.minFraction());
        System.out.println("Count greater than 4: " + fractionsList2.countGreaterThan(new Fraction(4, 1)));
        System.out.println("Count less than 4: " + fractionsList2.countLessThan(new Fraction(4, 1)));
        System.out.println("fractionsList2:");
        for (Fraction elem : fractionsList2.list) {
            System.out.println(elem);
        }
        System.out.println();

        FractionsList fractionsList3 = new FractionsList();
        fractionsList3.addFraction(new Fraction(3, 4));
        fractionsList3.addFraction(new Fraction(2, 3));
        fractionsList3.addFraction(new Fraction(4, 8));
        System.out.println("fractionsList3:");
        for (Fraction elem : fractionsList3.list) {
            System.out.println(elem);
        }
        System.out.println();
        Polynomial polynomial2 = new Polynomial(fractionsList2);
        Polynomial polynomial3 = new Polynomial(fractionsList3);
        Polynomial polynomial4 = polynomial2.addPolynomial(polynomial3);
        System.out.println(polynomial2);
        System.out.println(polynomial3);
        System.out.println(polynomial4);
    }
}