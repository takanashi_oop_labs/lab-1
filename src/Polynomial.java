public class Polynomial {
    private
    final
    FractionsList coefficients;

    Polynomial(FractionsList listOfFractions) {
        coefficients = new FractionsList(listOfFractions);
    }

    Polynomial addPolynomial(Polynomial polynomial) {
        Polynomial result = new Polynomial(this.coefficients);
        for (int i = 0; i < Math.max(polynomial.coefficients.size(), result.coefficients.size()); i++) {
            if (i >= polynomial.coefficients.size()) {

            } else if (i >= result.coefficients.size()) {
                result.coefficients.add(polynomial.coefficients.get(i));
            } else {
                result.coefficients.set(i, result.coefficients.get(i).addFraction(polynomial.coefficients.get(i)));
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String polynomial = "";
        for (int i = coefficients.size() - 1; i > 0; i--) {
            polynomial += coefficients.get(i).toString() + "x^" + i;
            polynomial += " + ";
        }
        polynomial += coefficients.get(0).toString();
        return String.format("Polynomial{%s}", polynomial);
    }
}