public class Fraction implements Comparable<Fraction> {
    private int numerator;
    private int denominator;

    Fraction(Integer m, Integer n) {
        int gcd = utils.GCD(m, n);
        this.numerator = m / gcd;
        this.denominator = n / gcd;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    Fraction addFraction(Fraction fraction) {
        return new Fraction(numerator * fraction.denominator + fraction.numerator * denominator,
                denominator * fraction.denominator);
    }

    @Override
    public String toString() {
        if (denominator != 1)
            return String.format("%d/%d", numerator, denominator);
        return String.format("%d", numerator);
    }

    @Override
    public int compareTo(Fraction o) {
        return numerator * o.denominator - o.numerator * denominator;
    }
}